-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2021 at 06:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sa_gvt`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodations`
--

CREATE TABLE `accomodations` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Rooms` int(11) DEFAULT NULL,
  `Breakfast` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Lunch` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dinner` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Single_Bed` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Double` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accomodations`
--

INSERT INTO `accomodations` (`id`, `Name`, `Address`, `Rooms`, `Breakfast`, `Lunch`, `Dinner`, `Single_Bed`, `Double`, `created_at`, `updated_at`) VALUES
(1, 'Jamerson Hotel', 'Cnr Chinhoyi Samora', 21, '$2.00', '$5.00', '$45.00', '$20', '$10', '2021-04-02 19:58:39', '2021-04-02 19:58:39'),
(2, 'Strand', 'wweff', 21, '$2.00', '$5.00', '$45.00', '$20', '$10', '2021-04-02 21:05:15', '2021-04-02 21:05:15');

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_checkins`
--

CREATE TABLE `accomodation_checkins` (
  `id` int(10) UNSIGNED NOT NULL,
  `Accomodation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Vistor_Name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `airports`
--

CREATE TABLE `airports` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `airports`
--

INSERT INTO `airports` (`id`, `name`, `created_at`, `updated_at`, `country`) VALUES
(1, 'Cape Town International Airport', '2021-04-03 08:51:09', '2021-04-03 08:51:09', '2'),
(2, 'Robert Mugabe International Airport', '2021-04-03 08:51:44', '2021-04-03 08:51:44', '1');

-- --------------------------------------------------------

--
-- Table structure for table `air_flights`
--

CREATE TABLE `air_flights` (
  `id` int(10) UNSIGNED NOT NULL,
  `flight_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dpt_airpot` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dest_airport` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_dpt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `air_flights`
--

INSERT INTO `air_flights` (`id`, `flight_number`, `dpt_airpot`, `dest_airport`, `date_dpt`, `created_at`, `updated_at`) VALUES
(2, NULL, '2', '1', NULL, '2021-04-03 08:57:52', '2021-04-03 08:57:52');

-- --------------------------------------------------------

--
-- Table structure for table `bill_backs`
--

CREATE TABLE `bill_backs` (
  `id` int(10) UNSIGNED NOT NULL,
  `vistor_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_offered` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt` longblob DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_backs`
--

INSERT INTO `bill_backs` (`id`, `vistor_id`, `service_offered`, `price`, `receipt`, `created_at`, `updated_at`) VALUES
(1, '23-345435G54', 'Accomodation', 'R123', 0x31323333343332343334, '2021-04-06 18:18:45', '2021-04-06 18:18:45'),
(2, '23-345435G54', 'Travelling', 'R50', 0x31323333343332343334, '2021-04-06 18:19:16', '2021-04-06 18:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Zimbabwe', '2021-03-27 17:33:34', '2021-03-27 17:33:34'),
(2, 'South Africa', '2021-04-02 19:39:15', '2021-04-02 19:39:15');

-- --------------------------------------------------------

--
-- Table structure for table `country_vistors`
--

CREATE TABLE `country_vistors` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Id_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_origin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country_vistors`
--

INSERT INTO `country_vistors` (`id`, `title`, `fullname`, `identity_type`, `Id_number`, `email`, `phone`, `gender`, `country_origin`, `created_at`, `updated_at`) VALUES
(1, '1', 'Roy Muzorori', '2', 'GN236475', 'roy@gmail.com', '0779755462', 'Male', '2', '2021-04-03 07:03:25', '2021-04-03 07:03:25'),
(2, '1', 'Officer Bellic', '1', '32-456434N23', 'bellic@gmail.com', '0779755462', 'Male', '2', '2021-04-03 07:45:49', '2021-04-03 07:45:49'),
(3, '1', 'Lincon Burrows', '1', '12321312', 'wewqeqwe', '3123', 'Male', '2', '2021-04-03 07:57:56', '2021-04-03 07:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(24, 7, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(48, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(49, 11, 'Name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(50, 11, 'Address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 3),
(51, 11, 'Rooms', 'text', 'Rooms', 0, 1, 1, 1, 1, 1, '{}', 4),
(52, 11, 'Breakfast', 'text', 'Breakfast', 0, 1, 1, 1, 1, 1, '{}', 5),
(53, 11, 'Lunch', 'text', 'Lunch', 0, 1, 1, 1, 1, 1, '{}', 6),
(54, 11, 'Dinner', 'text', 'Dinner', 0, 1, 1, 1, 1, 1, '{}', 7),
(55, 11, 'Single_Bed', 'text', 'Single Bed', 0, 1, 1, 1, 1, 1, '{}', 8),
(56, 11, 'Double', 'text', 'Double', 0, 1, 1, 1, 1, 1, '{}', 9),
(57, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(58, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(59, 1, 'user_hasmany_title_relationship', 'relationship', 'titles', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\UserTitle\",\"table\":\"titles\",\"type\":\"hasMany\",\"column\":\"name\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodationbookings\",\"pivot\":\"0\",\"taggable\":null}', 13),
(68, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(69, 20, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(71, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(72, 21, 'Accomodation', 'text', 'Accomodation', 0, 1, 1, 1, 1, 1, '{}', 2),
(73, 21, 'Vistor_Name', 'text', 'Vistor Name', 0, 1, 1, 1, 1, 1, '{}', 3),
(75, 21, 'accomodation_checkin_belongsto_vistor_relationship', 'relationship', 'vistors', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\CountryVistor\",\"table\":\"accomodation_checkins\",\"type\":\"belongsTo\",\"column\":\"Vistor_Name\",\"key\":\"id\",\"label\":\"vistor_name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(76, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(77, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(78, 21, 'accomodation_checkin_belongsto_accomodation_relationship', 'relationship', 'accomodations', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Accomodation\",\"table\":\"accomodations\",\"type\":\"belongsTo\",\"column\":\"Accomodation\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(93, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(94, 24, 'registration_number', 'text', 'Registration Number', 0, 1, 1, 1, 1, 1, '{}', 2),
(95, 24, 'make', 'text', 'Make', 0, 1, 1, 1, 1, 1, '{}', 3),
(96, 24, 'model', 'text', 'Model', 0, 1, 1, 1, 1, 1, '{}', 4),
(97, 24, 'driver_name', 'text', 'Driver Name', 0, 1, 1, 1, 1, 1, '{}', 5),
(98, 24, 'driver_id', 'text', 'Driver Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(99, 24, 'color', 'text', 'Color', 0, 1, 1, 1, 1, 1, '{}', 7),
(100, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(101, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(102, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(103, 25, 'vehicle_registration_number', 'text', 'Vehicle Registration Number', 0, 1, 1, 1, 1, 1, '{}', 2),
(104, 25, 'vistor_id_number', 'text', 'Vistor Id Number', 0, 1, 1, 1, 1, 1, '{}', 3),
(105, 25, 'vistor_destination', 'text', 'Vistor Destination', 0, 1, 1, 1, 1, 1, '{}', 4),
(106, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(107, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(108, 25, 'vehicle_booking_belongsto_registered_vehicle_relationship', 'relationship', 'registered_vehicles', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\RegisteredVehicle\",\"table\":\"registered_vehicles\",\"type\":\"belongsTo\",\"column\":\"vehicle_registration_number\",\"key\":\"id\",\"label\":\"registration_number\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(109, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(110, 26, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 2),
(111, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(112, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(124, 28, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(125, 28, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(126, 28, 'fullname', 'text', 'Fullname', 0, 1, 1, 1, 1, 1, '{}', 4),
(127, 28, 'identity_type', 'text', 'Identity Type', 0, 1, 1, 1, 1, 1, '{}', 6),
(128, 28, 'Id_number', 'text', 'Id Number', 0, 1, 1, 1, 1, 1, '{}', 7),
(129, 28, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 8),
(130, 28, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, '{}', 9),
(131, 28, 'gender', 'text', 'Gender', 0, 1, 1, 1, 1, 1, '{}', 10),
(132, 28, 'country_origin', 'text', 'Country Origin', 0, 1, 1, 1, 1, 1, '{}', 12),
(133, 28, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(134, 28, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(135, 28, 'country_vistor_belongsto_country_relationship', 'relationship', 'countries', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"country_origin\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(136, 28, 'country_vistor_belongsto_identity_type_relationship', 'relationship', 'identity_types', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\IdentityType\",\"table\":\"identity_types\",\"type\":\"belongsTo\",\"column\":\"identity_type\",\"key\":\"id\",\"label\":\"type\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(137, 28, 'country_vistor_belongsto_person_title_relationship', 'relationship', 'person_titles', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\PersonTitle\",\"table\":\"person_titles\",\"type\":\"belongsTo\",\"column\":\"title\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(138, 25, 'vehicle_booking_belongsto_country_vistor_relationship', 'relationship', 'country_vistors', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\CountryVistor\",\"table\":\"country_vistors\",\"type\":\"belongsTo\",\"column\":\"vistor_id_number\",\"key\":\"id\",\"label\":\"id_number\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(139, 35, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(140, 35, 'flight_number', 'text', 'Flight Number', 0, 1, 1, 1, 1, 1, '{}', 2),
(141, 35, 'dpt_airpot', 'text', 'Dpt Airpot', 0, 1, 1, 1, 1, 1, '{}', 3),
(142, 35, 'dest_airport', 'text', 'Dest Airport', 0, 1, 1, 1, 1, 1, '{}', 5),
(143, 35, 'date_dpt', 'text', 'Date Dpt', 0, 1, 1, 1, 1, 1, '{}', 7),
(144, 35, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(145, 35, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(150, 37, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(151, 37, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(152, 37, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(153, 37, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(154, 37, 'country', 'text', 'Country', 0, 1, 1, 1, 1, 1, '{}', 3),
(155, 37, 'airport_belongsto_country_relationship', 'relationship', 'countries', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"country\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(156, 35, 'air_flight_belongsto_airport_relationship', 'relationship', 'airports', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Airport\",\"table\":\"airports\",\"type\":\"belongsTo\",\"column\":\"dpt_airpot\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(157, 35, 'air_flight_belongsto_airport_relationship_1', 'relationship', 'airports', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Airport\",\"table\":\"airports\",\"type\":\"belongsTo\",\"column\":\"dest_airport\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accomodation_checkins\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(158, 38, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(159, 38, 'supplier_name', 'text', 'Supplier Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(160, 38, 'email_address', 'text', 'Email Address', 0, 1, 1, 1, 1, 1, '{}', 3),
(161, 38, 'phone_number', 'text', 'Phone Number', 0, 1, 1, 1, 1, 1, '{}', 4),
(162, 38, 'Industry', 'text', 'Industry', 0, 1, 1, 1, 1, 1, '{}', 5),
(163, 38, 'product_supplied', 'text', 'Product Supplied', 0, 1, 1, 1, 1, 1, '{}', 6),
(164, 38, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(165, 38, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(166, 39, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(167, 39, 'pick_up_point', 'text', 'Pick Up Point', 0, 1, 1, 1, 1, 1, '{}', 3),
(168, 39, 'destination', 'text', 'Destination', 0, 1, 1, 1, 1, 1, '{}', 4),
(169, 39, 'kilometres_travelled', 'text', 'Kilometres Travelled', 0, 1, 1, 1, 1, 1, '{}', 6),
(170, 39, 'Receipt', 'file', 'Receipt', 0, 1, 1, 1, 1, 1, '{}', 7),
(171, 39, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(172, 39, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(173, 39, 'traveller_id', 'text', 'Traveller Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(174, 39, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 5),
(175, 43, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(176, 43, 'vistor_id', 'text', 'Vistor Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(177, 43, 'service_offered', 'text', 'Service Offered', 0, 1, 1, 1, 1, 1, '{}', 3),
(178, 43, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 4),
(179, 43, 'receipt', 'text', 'Receipt', 0, 1, 1, 1, 1, 1, '{}', 5),
(180, 43, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(181, 43, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(182, 44, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(183, 44, 'vistor_id', 'text', 'Vistor Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(184, 44, 'service_offered', 'text', 'Service Offered', 0, 1, 1, 1, 1, 1, '{}', 3),
(185, 44, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 4),
(186, 44, 'receipt', 'text', 'Receipt', 0, 1, 1, 1, 1, 1, '{}', 5),
(187, 44, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(188, 44, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-03-27 16:25:39', '2021-03-27 16:25:39'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-03-27 16:25:39', '2021-03-27 16:25:39'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-03-27 16:25:39', '2021-03-27 16:25:39'),
(7, 'countries', 'countries', 'Country', 'Countries', 'voyager-company', 'App\\Country', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-03-27 17:28:13', '2021-03-27 17:32:06'),
(11, 'accomodations', 'accomodations', 'Accomodation', 'Accomodations', 'voyager-font', 'App\\Accomodation', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-02 16:12:08', '2021-04-02 16:12:08'),
(20, 'person_titles', 'person-titles', 'Person Title', 'Person Titles', 'voyager-settings', 'App\\PersonTitle', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-02 20:27:24', '2021-04-02 20:27:24'),
(21, 'accomodation_checkins', 'accomodation-checkins', 'Accomodation Checkin', 'Accomodation Checkins', 'voyager-calendar', 'App\\AccomodationCheckin', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-02 20:46:25', '2021-04-03 07:54:59'),
(24, 'registered_vehicles', 'registered-vehicles', 'Registered Vehicle', 'Registered Vehicles', 'voyager-leaf', 'App\\RegisteredVehicle', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-03 05:55:47', '2021-04-03 05:55:47'),
(25, 'vehicle_bookings', 'vehicle-bookings', 'Vehicle Booking', 'Vehicle Bookings', 'voyager-truck', 'App\\VehicleBooking', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-03 06:10:50', '2021-04-03 07:12:53'),
(26, 'identity_types', 'identity-types', 'Identity Type', 'Identity Types', 'voyager-list', 'App\\IdentityType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-03 06:25:03', '2021-04-03 06:25:03'),
(28, 'country_vistors', 'country-vistors', 'Country Vistor', 'Country Vistors', 'voyager-ship', 'App\\CountryVistor', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-03 06:34:03', '2021-04-03 06:44:03'),
(32, 'gender', 'gender', 'Gender', 'Genders', 'voyager-settings', 'App\\Gender', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-03 06:59:55', '2021-04-03 06:59:55'),
(35, 'air_flights', 'air-flights', 'Air Flight', 'Air Flights', 'voyager-rocket', 'App\\AirFlight', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-03 08:37:25', '2021-04-03 08:57:30'),
(37, 'airports', 'airports', 'Airport', 'Airports', 'voyager-world', 'App\\Airport', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-03 08:48:11', '2021-04-03 08:53:06'),
(38, 'suppliers', 'suppliers', 'Supplier', 'Suppliers', 'voyager-bag', 'App\\Supplier', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-06 17:24:11', '2021-04-06 17:24:11'),
(39, 'travel_expense_management', 'travel-expense-management', 'Travel Expense Management', 'Travel Expense Managements', 'voyager-credit-card', 'App\\TravelExpenseManagement', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-06 17:37:50', '2021-04-06 17:54:13'),
(41, 'bill_back', 'bill-back', 'Bill Back', 'Bill Back', 'voyager-logbook', 'App\\BillBack', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-06 17:59:46', '2021-04-06 18:02:53'),
(42, 'bill', 'bill', 'Bill', 'Bills', 'voyager-logbook', 'App\\Bill', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-06 18:05:40', '2021-04-06 18:05:40'),
(43, 'bills', 'bills', 'Bill', 'Bills', 'voyager-logbook', 'App\\Bill', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-06 18:07:16', '2021-04-06 18:07:16'),
(44, 'bill_backs', 'bill-backs', 'Bill Back', 'Bill Backs', 'voyager-logbook', 'App\\BillBack', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-06 18:09:28', '2021-04-06 18:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `identity_types`
--

CREATE TABLE `identity_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `identity_types`
--

INSERT INTO `identity_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Nation ID', '2021-04-03 06:25:30', '2021-04-03 06:25:30'),
(2, 'Passport', '2021-04-03 06:25:44', '2021-04-03 06:25:44'),
(3, 'Drivers Licence', '2021-04-03 06:26:05', '2021-04-03 06:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-03-27 16:25:41', '2021-03-27 16:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-03-27 16:25:41', '2021-03-27 16:25:41', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2021-03-27 16:25:42', '2021-03-27 16:25:42', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2021-03-27 16:25:42', '2021-03-27 16:25:42', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2021-03-27 16:25:46', '2021-03-27 16:25:46', 'voyager.hooks', NULL),
(12, 1, 'Countries', '', '_self', NULL, NULL, NULL, 15, '2021-03-27 17:28:14', '2021-03-27 17:28:14', 'voyager.countries.index', NULL),
(16, 1, 'Accomodations', '', '_self', 'voyager-font', NULL, NULL, 19, '2021-04-02 16:12:09', '2021-04-02 16:12:09', 'voyager.accomodations.index', NULL),
(24, 1, 'Person Titles', '', '_self', 'voyager-settings', NULL, NULL, 24, '2021-04-02 20:27:25', '2021-04-02 20:27:25', 'voyager.person-titles.index', NULL),
(25, 1, 'Accomodation Checkins', '', '_self', 'voyager-calendar', NULL, NULL, 25, '2021-04-02 20:46:26', '2021-04-02 20:46:26', 'voyager.accomodation-checkins.index', NULL),
(28, 1, 'Registered Vehicles', '', '_self', 'voyager-leaf', NULL, NULL, 26, '2021-04-03 05:55:48', '2021-04-03 05:55:48', 'voyager.registered-vehicles.index', NULL),
(29, 1, 'Vehicle Bookings', '', '_self', 'voyager-truck', NULL, NULL, 27, '2021-04-03 06:10:51', '2021-04-03 06:10:51', 'voyager.vehicle-bookings.index', NULL),
(30, 1, 'Identity Types', '', '_self', 'voyager-list', NULL, NULL, 28, '2021-04-03 06:25:03', '2021-04-03 06:25:03', 'voyager.identity-types.index', NULL),
(32, 1, 'Country Vistors', '', '_self', 'voyager-ship', NULL, NULL, 29, '2021-04-03 06:34:03', '2021-04-03 06:34:03', 'voyager.country-vistors.index', NULL),
(35, 1, 'Genders', '', '_self', 'voyager-settings', NULL, NULL, 30, '2021-04-03 06:59:55', '2021-04-03 06:59:55', 'voyager.gender.index', NULL),
(37, 1, 'Air Flights', '', '_self', 'voyager-rocket', NULL, NULL, 31, '2021-04-03 08:37:26', '2021-04-03 08:37:26', 'voyager.air-flights.index', NULL),
(39, 1, 'Airports', '', '_self', 'voyager-world', NULL, NULL, 32, '2021-04-03 08:48:12', '2021-04-03 08:48:12', 'voyager.airports.index', NULL),
(40, 1, 'Suppliers', '', '_self', 'voyager-bag', NULL, NULL, 33, '2021-04-06 17:24:12', '2021-04-06 17:24:12', 'voyager.suppliers.index', NULL),
(41, 1, 'Travel Expense Managements', '', '_self', 'voyager-credit-card', NULL, NULL, 34, '2021-04-06 17:37:51', '2021-04-06 17:37:51', 'voyager.travel-expense-management.index', NULL),
(43, 1, 'Bill Back', '', '_self', 'voyager-logbook', NULL, NULL, 35, '2021-04-06 17:59:47', '2021-04-06 17:59:47', 'voyager.bill-back.index', NULL),
(44, 1, 'Bills', '', '_self', 'voyager-logbook', NULL, NULL, 36, '2021-04-06 18:05:41', '2021-04-06 18:05:41', 'voyager.bill.index', NULL),
(45, 1, 'Bills', '', '_self', 'voyager-logbook', NULL, NULL, 37, '2021-04-06 18:07:17', '2021-04-06 18:07:17', 'voyager.bills.index', NULL),
(46, 1, 'Bill Backs', '', '_self', 'voyager-logbook', NULL, NULL, 38, '2021-04-06 18:09:28', '2021-04-06 18:09:28', 'voyager.bill-backs.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_01_000000_add_voyager_user_fields', 1),
(3, '2016_01_01_000000_create_data_types_table', 1),
(4, '2016_05_19_173453_create_menu_table', 1),
(5, '2016_10_21_190000_create_roles_table', 1),
(6, '2016_10_21_190000_create_settings_table', 1),
(7, '2016_11_30_135954_create_permission_table', 1),
(8, '2016_11_30_141208_create_permission_role_table', 1),
(9, '2016_12_26_201236_data_types__add__server_side', 1),
(10, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(11, '2017_01_14_005015_create_translations_table', 1),
(12, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(13, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(14, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(15, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(16, '2017_08_05_000000_add_group_to_settings_table', 1),
(17, '2017_11_26_013050_add_user_role_relationship', 1),
(18, '2017_11_26_015000_create_user_roles_table', 1),
(19, '2018_03_11_000000_add_user_settings', 1),
(20, '2018_03_14_000000_add_details_to_data_types_table', 1),
(21, '2018_03_16_000000_make_settings_value_nullable', 1),
(22, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-03-27 16:25:42', '2021-03-27 16:25:42'),
(2, 'browse_bread', NULL, '2021-03-27 16:25:42', '2021-03-27 16:25:42'),
(3, 'browse_database', NULL, '2021-03-27 16:25:42', '2021-03-27 16:25:42'),
(4, 'browse_media', NULL, '2021-03-27 16:25:42', '2021-03-27 16:25:42'),
(5, 'browse_compass', NULL, '2021-03-27 16:25:42', '2021-03-27 16:25:42'),
(6, 'browse_menus', 'menus', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(7, 'read_menus', 'menus', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(8, 'edit_menus', 'menus', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(9, 'add_menus', 'menus', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(10, 'delete_menus', 'menus', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(11, 'browse_roles', 'roles', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(12, 'read_roles', 'roles', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(13, 'edit_roles', 'roles', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(14, 'add_roles', 'roles', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(15, 'delete_roles', 'roles', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(16, 'browse_users', 'users', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(17, 'read_users', 'users', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(18, 'edit_users', 'users', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(19, 'add_users', 'users', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(20, 'delete_users', 'users', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(21, 'browse_settings', 'settings', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(22, 'read_settings', 'settings', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(23, 'edit_settings', 'settings', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(24, 'add_settings', 'settings', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(25, 'delete_settings', 'settings', '2021-03-27 16:25:43', '2021-03-27 16:25:43'),
(26, 'browse_hooks', NULL, '2021-03-27 16:25:46', '2021-03-27 16:25:46'),
(27, 'browse_countries', 'countries', '2021-03-27 17:28:14', '2021-03-27 17:28:14'),
(28, 'read_countries', 'countries', '2021-03-27 17:28:14', '2021-03-27 17:28:14'),
(29, 'edit_countries', 'countries', '2021-03-27 17:28:14', '2021-03-27 17:28:14'),
(30, 'add_countries', 'countries', '2021-03-27 17:28:14', '2021-03-27 17:28:14'),
(31, 'delete_countries', 'countries', '2021-03-27 17:28:14', '2021-03-27 17:28:14'),
(47, 'browse_accomodations', 'accomodations', '2021-04-02 16:12:09', '2021-04-02 16:12:09'),
(48, 'read_accomodations', 'accomodations', '2021-04-02 16:12:09', '2021-04-02 16:12:09'),
(49, 'edit_accomodations', 'accomodations', '2021-04-02 16:12:09', '2021-04-02 16:12:09'),
(50, 'add_accomodations', 'accomodations', '2021-04-02 16:12:09', '2021-04-02 16:12:09'),
(51, 'delete_accomodations', 'accomodations', '2021-04-02 16:12:09', '2021-04-02 16:12:09'),
(87, 'browse_person_titles', 'person_titles', '2021-04-02 20:27:25', '2021-04-02 20:27:25'),
(88, 'read_person_titles', 'person_titles', '2021-04-02 20:27:25', '2021-04-02 20:27:25'),
(89, 'edit_person_titles', 'person_titles', '2021-04-02 20:27:25', '2021-04-02 20:27:25'),
(90, 'add_person_titles', 'person_titles', '2021-04-02 20:27:25', '2021-04-02 20:27:25'),
(91, 'delete_person_titles', 'person_titles', '2021-04-02 20:27:25', '2021-04-02 20:27:25'),
(92, 'browse_accomodation_checkins', 'accomodation_checkins', '2021-04-02 20:46:26', '2021-04-02 20:46:26'),
(93, 'read_accomodation_checkins', 'accomodation_checkins', '2021-04-02 20:46:26', '2021-04-02 20:46:26'),
(94, 'edit_accomodation_checkins', 'accomodation_checkins', '2021-04-02 20:46:26', '2021-04-02 20:46:26'),
(95, 'add_accomodation_checkins', 'accomodation_checkins', '2021-04-02 20:46:26', '2021-04-02 20:46:26'),
(96, 'delete_accomodation_checkins', 'accomodation_checkins', '2021-04-02 20:46:26', '2021-04-02 20:46:26'),
(107, 'browse_registered_vehicles', 'registered_vehicles', '2021-04-03 05:55:48', '2021-04-03 05:55:48'),
(108, 'read_registered_vehicles', 'registered_vehicles', '2021-04-03 05:55:48', '2021-04-03 05:55:48'),
(109, 'edit_registered_vehicles', 'registered_vehicles', '2021-04-03 05:55:48', '2021-04-03 05:55:48'),
(110, 'add_registered_vehicles', 'registered_vehicles', '2021-04-03 05:55:48', '2021-04-03 05:55:48'),
(111, 'delete_registered_vehicles', 'registered_vehicles', '2021-04-03 05:55:48', '2021-04-03 05:55:48'),
(112, 'browse_vehicle_bookings', 'vehicle_bookings', '2021-04-03 06:10:50', '2021-04-03 06:10:50'),
(113, 'read_vehicle_bookings', 'vehicle_bookings', '2021-04-03 06:10:50', '2021-04-03 06:10:50'),
(114, 'edit_vehicle_bookings', 'vehicle_bookings', '2021-04-03 06:10:50', '2021-04-03 06:10:50'),
(115, 'add_vehicle_bookings', 'vehicle_bookings', '2021-04-03 06:10:51', '2021-04-03 06:10:51'),
(116, 'delete_vehicle_bookings', 'vehicle_bookings', '2021-04-03 06:10:51', '2021-04-03 06:10:51'),
(117, 'browse_identity_types', 'identity_types', '2021-04-03 06:25:03', '2021-04-03 06:25:03'),
(118, 'read_identity_types', 'identity_types', '2021-04-03 06:25:03', '2021-04-03 06:25:03'),
(119, 'edit_identity_types', 'identity_types', '2021-04-03 06:25:03', '2021-04-03 06:25:03'),
(120, 'add_identity_types', 'identity_types', '2021-04-03 06:25:03', '2021-04-03 06:25:03'),
(121, 'delete_identity_types', 'identity_types', '2021-04-03 06:25:03', '2021-04-03 06:25:03'),
(127, 'browse_country_vistors', 'country_vistors', '2021-04-03 06:34:03', '2021-04-03 06:34:03'),
(128, 'read_country_vistors', 'country_vistors', '2021-04-03 06:34:03', '2021-04-03 06:34:03'),
(129, 'edit_country_vistors', 'country_vistors', '2021-04-03 06:34:03', '2021-04-03 06:34:03'),
(130, 'add_country_vistors', 'country_vistors', '2021-04-03 06:34:03', '2021-04-03 06:34:03'),
(131, 'delete_country_vistors', 'country_vistors', '2021-04-03 06:34:03', '2021-04-03 06:34:03'),
(142, 'browse_gender', 'gender', '2021-04-03 06:59:55', '2021-04-03 06:59:55'),
(143, 'read_gender', 'gender', '2021-04-03 06:59:55', '2021-04-03 06:59:55'),
(144, 'edit_gender', 'gender', '2021-04-03 06:59:55', '2021-04-03 06:59:55'),
(145, 'add_gender', 'gender', '2021-04-03 06:59:55', '2021-04-03 06:59:55'),
(146, 'delete_gender', 'gender', '2021-04-03 06:59:55', '2021-04-03 06:59:55'),
(152, 'browse_air_flights', 'air_flights', '2021-04-03 08:37:25', '2021-04-03 08:37:25'),
(153, 'read_air_flights', 'air_flights', '2021-04-03 08:37:26', '2021-04-03 08:37:26'),
(154, 'edit_air_flights', 'air_flights', '2021-04-03 08:37:26', '2021-04-03 08:37:26'),
(155, 'add_air_flights', 'air_flights', '2021-04-03 08:37:26', '2021-04-03 08:37:26'),
(156, 'delete_air_flights', 'air_flights', '2021-04-03 08:37:26', '2021-04-03 08:37:26'),
(162, 'browse_airports', 'airports', '2021-04-03 08:48:11', '2021-04-03 08:48:11'),
(163, 'read_airports', 'airports', '2021-04-03 08:48:11', '2021-04-03 08:48:11'),
(164, 'edit_airports', 'airports', '2021-04-03 08:48:11', '2021-04-03 08:48:11'),
(165, 'add_airports', 'airports', '2021-04-03 08:48:11', '2021-04-03 08:48:11'),
(166, 'delete_airports', 'airports', '2021-04-03 08:48:11', '2021-04-03 08:48:11'),
(167, 'browse_suppliers', 'suppliers', '2021-04-06 17:24:11', '2021-04-06 17:24:11'),
(168, 'read_suppliers', 'suppliers', '2021-04-06 17:24:11', '2021-04-06 17:24:11'),
(169, 'edit_suppliers', 'suppliers', '2021-04-06 17:24:11', '2021-04-06 17:24:11'),
(170, 'add_suppliers', 'suppliers', '2021-04-06 17:24:11', '2021-04-06 17:24:11'),
(171, 'delete_suppliers', 'suppliers', '2021-04-06 17:24:11', '2021-04-06 17:24:11'),
(172, 'browse_travel_expense_management', 'travel_expense_management', '2021-04-06 17:37:51', '2021-04-06 17:37:51'),
(173, 'read_travel_expense_management', 'travel_expense_management', '2021-04-06 17:37:51', '2021-04-06 17:37:51'),
(174, 'edit_travel_expense_management', 'travel_expense_management', '2021-04-06 17:37:51', '2021-04-06 17:37:51'),
(175, 'add_travel_expense_management', 'travel_expense_management', '2021-04-06 17:37:51', '2021-04-06 17:37:51'),
(176, 'delete_travel_expense_management', 'travel_expense_management', '2021-04-06 17:37:51', '2021-04-06 17:37:51'),
(182, 'browse_bill_back', 'bill_back', '2021-04-06 17:59:46', '2021-04-06 17:59:46'),
(183, 'read_bill_back', 'bill_back', '2021-04-06 17:59:47', '2021-04-06 17:59:47'),
(184, 'edit_bill_back', 'bill_back', '2021-04-06 17:59:47', '2021-04-06 17:59:47'),
(185, 'add_bill_back', 'bill_back', '2021-04-06 17:59:47', '2021-04-06 17:59:47'),
(186, 'delete_bill_back', 'bill_back', '2021-04-06 17:59:47', '2021-04-06 17:59:47'),
(187, 'browse_bill', 'bill', '2021-04-06 18:05:40', '2021-04-06 18:05:40'),
(188, 'read_bill', 'bill', '2021-04-06 18:05:40', '2021-04-06 18:05:40'),
(189, 'edit_bill', 'bill', '2021-04-06 18:05:40', '2021-04-06 18:05:40'),
(190, 'add_bill', 'bill', '2021-04-06 18:05:41', '2021-04-06 18:05:41'),
(191, 'delete_bill', 'bill', '2021-04-06 18:05:41', '2021-04-06 18:05:41'),
(192, 'browse_bills', 'bills', '2021-04-06 18:07:17', '2021-04-06 18:07:17'),
(193, 'read_bills', 'bills', '2021-04-06 18:07:17', '2021-04-06 18:07:17'),
(194, 'edit_bills', 'bills', '2021-04-06 18:07:17', '2021-04-06 18:07:17'),
(195, 'add_bills', 'bills', '2021-04-06 18:07:17', '2021-04-06 18:07:17'),
(196, 'delete_bills', 'bills', '2021-04-06 18:07:17', '2021-04-06 18:07:17'),
(197, 'browse_bill_backs', 'bill_backs', '2021-04-06 18:09:28', '2021-04-06 18:09:28'),
(198, 'read_bill_backs', 'bill_backs', '2021-04-06 18:09:28', '2021-04-06 18:09:28'),
(199, 'edit_bill_backs', 'bill_backs', '2021-04-06 18:09:28', '2021-04-06 18:09:28'),
(200, 'add_bill_backs', 'bill_backs', '2021-04-06 18:09:28', '2021-04-06 18:09:28'),
(201, 'delete_bill_backs', 'bill_backs', '2021-04-06 18:09:28', '2021-04-06 18:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(49, 2),
(50, 1),
(50, 2),
(51, 1),
(51, 2),
(87, 1),
(87, 2),
(88, 1),
(88, 2),
(89, 1),
(89, 2),
(90, 1),
(90, 2),
(91, 1),
(91, 2),
(92, 1),
(92, 2),
(93, 1),
(93, 2),
(94, 1),
(94, 2),
(95, 1),
(95, 2),
(96, 1),
(96, 2),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1),
(194, 1),
(195, 1),
(196, 1),
(197, 1),
(198, 1),
(199, 1),
(200, 1),
(201, 1);

-- --------------------------------------------------------

--
-- Table structure for table `person_titles`
--

CREATE TABLE `person_titles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `person_titles`
--

INSERT INTO `person_titles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Mr', '2021-04-02 20:28:49', '2021-04-02 20:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `registered_vehicles`
--

CREATE TABLE `registered_vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `registration_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `make` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `registered_vehicles`
--

INSERT INTO `registered_vehicles` (`id`, `registration_number`, `make`, `model`, `driver_name`, `driver_id`, `color`, `created_at`, `updated_at`) VALUES
(1, 'AEH-8604', 'Honda', 'Fit GD1', 'Lee Kaliyati', '32-189157N32', 'Yellow', '2021-04-03 06:16:50', '2021-04-03 06:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-03-27 16:25:42', '2021-03-27 16:25:42'),
(2, 'user', 'Normal User', '2021-03-27 16:25:42', '2021-03-27 16:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Travelling Management', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'As travelers management system', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Industry` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_supplied` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplier_name`, `email_address`, `phone_number`, `Industry`, `product_supplied`, `created_at`, `updated_at`) VALUES
(1, 'Rains', 'info@rains.co.za', '+263779755462', 'Telecomunication', 'Rains Sim Cards', '2021-04-06 18:13:19', '2021-04-06 18:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `travel_expense_management`
--

CREATE TABLE `travel_expense_management` (
  `id` int(10) UNSIGNED NOT NULL,
  `pick_up_point` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kilometres_travelled` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Receipt` longblob DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `traveller_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `travel_expense_management`
--

INSERT INTO `travel_expense_management` (`id`, `pick_up_point`, `destination`, `kilometres_travelled`, `Receipt`, `created_at`, `updated_at`, `traveller_id`, `price`) VALUES
(1, 'Strand Capetown', 'Gordons Bay Beach', '5 KM', 0x5b7b22646f776e6c6f61645f6c696e6b223a2274726176656c2d657870656e73652d6d616e6167656d656e745c5c417072696c323032315c5c49796d4c37556e316f3147506b4f7a7356735a762e6a7067222c226f726967696e616c5f6e616d65223a223565623938656565336461633961306464653630373631382e6a7067227d5d, '2021-04-06 18:16:10', '2021-04-06 18:16:10', '32-173817N23', 'R50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Lee Kaliyati', 'kaliyatilee@gmail.com', 'users/default.png', NULL, '$2y$10$gwao0VPb0PD20wMsp8uFc.iD1DaWcts6vsx5HLtFerwohdQ6AqhZW', NULL, NULL, '2021-03-27 16:29:36', '2021-03-27 16:29:36'),
(2, 2, 'Super Admin', 'sadmin@gmail.com', 'users\\April2021\\AsLLkbNA4rhKsCKBUqsZ.jpg', NULL, '$2y$10$JU.hQQdF5ea7AirAjcVdqOB9CSIr8vFmbbcEJfNLvl74rSuwEiQNW', NULL, '{\"locale\":\"en\"}', '2021-04-03 05:03:58', '2021-04-03 05:04:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_bookings`
--

CREATE TABLE `vehicle_bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_registration_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vistor_id_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vistor_destination` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_bookings`
--

INSERT INTO `vehicle_bookings` (`id`, `vehicle_registration_number`, `vistor_id_number`, `vistor_destination`, `created_at`, `updated_at`) VALUES
(1, '1', NULL, 'Kuwadzana 4  Harare', '2021-04-03 07:13:33', '2021-04-03 07:13:33'),
(2, '1', NULL, 'Norton', '2021-04-03 08:00:54', '2021-04-03 08:00:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomodations`
--
ALTER TABLE `accomodations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accomodation_checkins`
--
ALTER TABLE `accomodation_checkins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airports`
--
ALTER TABLE `airports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `air_flights`
--
ALTER TABLE `air_flights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_backs`
--
ALTER TABLE `bill_backs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_vistors`
--
ALTER TABLE `country_vistors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identity_types`
--
ALTER TABLE `identity_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `person_titles`
--
ALTER TABLE `person_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered_vehicles`
--
ALTER TABLE `registered_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `travel_expense_management`
--
ALTER TABLE `travel_expense_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `vehicle_bookings`
--
ALTER TABLE `vehicle_bookings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomodations`
--
ALTER TABLE `accomodations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `accomodation_checkins`
--
ALTER TABLE `accomodation_checkins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `airports`
--
ALTER TABLE `airports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `air_flights`
--
ALTER TABLE `air_flights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bill_backs`
--
ALTER TABLE `bill_backs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country_vistors`
--
ALTER TABLE `country_vistors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `identity_types`
--
ALTER TABLE `identity_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `person_titles`
--
ALTER TABLE `person_titles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registered_vehicles`
--
ALTER TABLE `registered_vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `travel_expense_management`
--
ALTER TABLE `travel_expense_management`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_bookings`
--
ALTER TABLE `vehicle_bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
